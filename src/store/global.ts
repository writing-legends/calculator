import { reactive, readonly } from "vue";

const state = reactive({
  memory: "_",
});

const saveToMemory = function (value: string) {
  state.memory = value;
};

export default { state: readonly(state), saveToMemory };
